package hung.phan.ghiblimoviescleanarchitecture.data.remote

import hung.phan.ghiblimoviescleanarchitecture.data.remote.dto.FilmDto
import retrofit2.http.GET
import retrofit2.http.Path

interface GhibliApi {
  @GET("/films")
  suspend fun getFilms() : List<FilmDto>

  @GET("/films/{filmId}")
  suspend fun getFilmById(@Path("filmId") filmId: String) : FilmDto
}