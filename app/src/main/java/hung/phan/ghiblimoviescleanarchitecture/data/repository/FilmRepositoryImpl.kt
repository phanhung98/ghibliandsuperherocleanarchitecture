package hung.phan.ghiblimoviescleanarchitecture.data.repository

import hung.phan.ghiblimoviescleanarchitecture.data.remote.GhibliApi
import hung.phan.ghiblimoviescleanarchitecture.data.remote.dto.FilmDto
import hung.phan.ghiblimoviescleanarchitecture.domain.repository.FilmRepository
import javax.inject.Inject

class FilmRepositoryImpl @Inject constructor(
  private val api: GhibliApi
): FilmRepository {
  override suspend fun getFilms(): List<FilmDto> {
    return api.getFilms()
  }

  override suspend fun getFilmById(filmId: String): FilmDto {
    return api.getFilmById(filmId)
  }
}