package hung.phan.ghiblimoviescleanarchitecture.presentation.film_detail

import hung.phan.ghiblimoviescleanarchitecture.domain.model.Film

data class FilmDetailState(
  val isLoading: Boolean = false,
  val films: Film? = null,
  val error: String = ""
)