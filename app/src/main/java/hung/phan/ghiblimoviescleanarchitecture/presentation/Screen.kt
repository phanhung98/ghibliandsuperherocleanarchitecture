package hung.phan.ghiblimoviescleanarchitecture.presentation

sealed class Screen(val route: String) {
  object FilmListScreen: Screen(route = "film_list_screen")

}
