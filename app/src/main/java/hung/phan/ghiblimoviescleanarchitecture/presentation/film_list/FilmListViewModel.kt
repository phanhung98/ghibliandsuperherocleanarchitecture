package hung.phan.ghiblimoviescleanarchitecture.presentation.film_list

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import hung.phan.ghiblimoviescleanarchitecture.common.Resource
import hung.phan.ghiblimoviescleanarchitecture.domain.use_case.get_films.GetFilmsUseCase
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class FilmListViewModel @Inject constructor(
  private val getFilmsUseCase: GetFilmsUseCase
) : ViewModel() {

  private val _state = mutableStateOf(FilmListState())
  val state: State<FilmListState> = _state

  init {
    getFilms()
  }

  private fun getFilms() {
    getFilmsUseCase().onEach { result ->
      when(result) {
        is Resource.Success -> {
          _state.value = FilmListState(films = result.data ?: emptyList())
        }
        is Resource.Error -> {
          _state.value = FilmListState(
            error = result.message ?: "An unexpected error"
          )
        }
        is Resource.Loading -> {
          _state.value = FilmListState(isLoading = true)
        }
      }
    }.launchIn(viewModelScope)
  }
}