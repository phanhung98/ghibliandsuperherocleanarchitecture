package hung.phan.ghiblimoviescleanarchitecture.presentation.film_list

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import hung.phan.ghiblimoviescleanarchitecture.domain.model.Film
import hung.phan.ghiblimoviescleanarchitecture.presentation.film_list.components.FilmListItem

@Composable
fun FilmsScreen(
  navController: NavController,
  viewModel: FilmListViewModel = hiltViewModel()
) {
  val state = viewModel.state.value
  Box(modifier = Modifier.fillMaxSize()) {
    LazyColumn(modifier = Modifier.fillMaxSize()) {
      items(state.films) { film ->
        Box(modifier = Modifier.fillMaxWidth(0.5f).padding(16.dp)) {
          FilmListItem(film = film, onItemClick = {
//          navController.navigate()
          })
        }
      }
    }
    if (state.error.isBlank()) {
      Text(text = state.error, color = MaterialTheme.colors.error,
      textAlign = TextAlign.Center,
      modifier = Modifier
        .fillMaxWidth()
        .padding(horizontal = 20.dp)
        .align(Companion.Center))
    }
    if (state.isLoading) {
      CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
    }
  }
}