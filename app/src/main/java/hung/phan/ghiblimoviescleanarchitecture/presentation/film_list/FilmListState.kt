package hung.phan.ghiblimoviescleanarchitecture.presentation.film_list

import hung.phan.ghiblimoviescleanarchitecture.common.Resource.Error
import hung.phan.ghiblimoviescleanarchitecture.domain.model.Film

data class FilmListState (
  val isLoading: Boolean = false,
  val films: List<Film> = emptyList(),
  val error: String = ""
)