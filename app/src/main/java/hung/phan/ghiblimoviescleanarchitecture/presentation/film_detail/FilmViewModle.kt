package hung.phan.ghiblimoviescleanarchitecture.presentation.film_detail

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import hung.phan.ghiblimoviescleanarchitecture.common.Constants
import hung.phan.ghiblimoviescleanarchitecture.common.Resource
import hung.phan.ghiblimoviescleanarchitecture.domain.use_case.get_film.GetFilmUseCase
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class FilmViewModel @Inject constructor(
  private val getFilmUseCase: GetFilmUseCase,
  savedStateHandle: SavedStateHandle
) : ViewModel() {

  private val _state = mutableStateOf(FilmDetailState())
  val state: State<FilmDetailState> = _state

  init {
    savedStateHandle.get<String>(Constants.PARAM_FILM_ID)?.let { filmId ->
      getFilm(filmId)
    }
  }

  private fun getFilm(filmId: String) {
    getFilmUseCase(filmId).onEach { result ->
      when(result) {
        is Resource.Success -> {
          _state.value = FilmDetailState(films = result.data)
        }
        is Resource.Error -> {
          _state.value = FilmDetailState(
            error = result.message ?: "An unexpected error"
          )
        }
        is Resource.Loading -> {
          _state.value = FilmDetailState(isLoading = true)
        }
      }
    }.launchIn(viewModelScope)
  }
}