package hung.phan.ghiblimoviescleanarchitecture.presentation.film_list.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.ContentScale.Companion
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.compose.rememberAsyncImagePainter
import hung.phan.ghiblimoviescleanarchitecture.domain.model.Film

@Composable
fun FilmListItem(
  film: Film,
  onItemClick: (Film) -> Unit
) {
  Card(modifier = Modifier.fillMaxWidth(),
  shape = RoundedCornerShape(15.dp),
  elevation = 6.dp) {
    Box(modifier = Modifier.height(250.dp)) {
      val painter = rememberAsyncImagePainter(model = film.image)
      Image(painter = painter, contentDescription = "Image item",
        contentScale = ContentScale.Crop)
      Box(modifier = Modifier
        .fillMaxSize()
        .background(
          Brush.verticalGradient(
            colors = listOf(
              Color.Transparent,
              Color.Black
            ),
            startY = 350f
          )
        ))
      Box(modifier = Modifier
        .fillMaxSize()
        .padding(12.dp),
        contentAlignment = Alignment.BottomStart
      ) {
        Text(text = film.original_title_romanised, style = TextStyle(color = Color.White, fontSize = 16.sp))
      }
    }
  }
}