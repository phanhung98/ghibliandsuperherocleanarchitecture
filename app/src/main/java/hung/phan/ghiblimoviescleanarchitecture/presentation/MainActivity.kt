package hung.phan.ghiblimoviescleanarchitecture.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import dagger.hilt.android.AndroidEntryPoint
import hung.phan.ghiblimoviescleanarchitecture.presentation.Screen.FilmListScreen
import hung.phan.ghiblimoviescleanarchitecture.presentation.film_list.FilmsScreen
import hung.phan.ghiblimoviescleanarchitecture.presentation.ui.theme.GhibliMoviesCleanArchitectureTheme

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContent {
      GhibliMoviesCleanArchitectureTheme {
        // A surface container using the 'background' color from the theme
        Surface(color = MaterialTheme.colors.background) {
         val navController = rememberNavController()
          NavHost(navController = navController, startDestination = Screen.FilmListScreen.route ) {
            composable(route = Screen.FilmListScreen.route) {
              FilmsScreen(navController = navController)
            }
          }
        }
      }
    }
  }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
  GhibliMoviesCleanArchitectureTheme {
  }
}