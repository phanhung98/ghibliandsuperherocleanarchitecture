package hung.phan.ghiblimoviescleanarchitecture.common

object Constants {
  const val BASE_URL = "https://ghibliapi.herokuapp.com"
  const val PARAM_FILM_ID = "filmId"
}