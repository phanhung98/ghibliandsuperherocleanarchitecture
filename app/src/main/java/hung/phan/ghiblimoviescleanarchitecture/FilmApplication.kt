package hung.phan.ghiblimoviescleanarchitecture

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FilmApplication : Application() {
}