package hung.phan.ghiblimoviescleanarchitecture.domain.repository

import hung.phan.ghiblimoviescleanarchitecture.data.remote.dto.FilmDto
import hung.phan.ghiblimoviescleanarchitecture.domain.model.Film

interface FilmRepository {

  suspend fun getFilms(): List<FilmDto>

  suspend fun getFilmById(filmId: String) : FilmDto
}