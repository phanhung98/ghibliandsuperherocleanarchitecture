package hung.phan.ghiblimoviescleanarchitecture.domain.use_case.get_film

import hung.phan.ghiblimoviescleanarchitecture.common.Resource
import hung.phan.ghiblimoviescleanarchitecture.common.Resource.Error
import hung.phan.ghiblimoviescleanarchitecture.common.Resource.Loading
import hung.phan.ghiblimoviescleanarchitecture.common.Resource.Success
import hung.phan.ghiblimoviescleanarchitecture.data.remote.dto.toFilm
import hung.phan.ghiblimoviescleanarchitecture.domain.model.Film
import hung.phan.ghiblimoviescleanarchitecture.domain.repository.FilmRepository
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetFilmUseCase @Inject constructor(
  private val repository: FilmRepository
){
  operator fun invoke(filmId: String): kotlinx.coroutines.flow.Flow<Resource<Film>> = flow {
    try {
      emit(Loading())
      val film = repository.getFilmById(filmId).toFilm()
      emit(Success(film))
    } catch (e: HttpException) {
      emit(Error(e.localizedMessage ?:"An unexpected error occured" ))
    } catch (e: IOException) {
      emit(Error("Couldn't reach server"))
    }
  }
}