package hung.phan.ghiblimoviescleanarchitecture.domain.use_case.get_films

import hung.phan.ghiblimoviescleanarchitecture.common.Resource
import hung.phan.ghiblimoviescleanarchitecture.data.remote.dto.toFilm
import hung.phan.ghiblimoviescleanarchitecture.domain.model.Film
import hung.phan.ghiblimoviescleanarchitecture.domain.repository.FilmRepository
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetFilmsUseCase @Inject constructor(
  private val repository: FilmRepository
){
  operator fun invoke(): kotlinx.coroutines.flow.Flow<Resource<List<Film>>> = flow {
    try {
      emit(Resource.Loading())
      val films = repository.getFilms().map { it.toFilm() }
      emit(Resource.Success(films))
    } catch (e: HttpException) {
      emit(Resource.Error(e.localizedMessage ?:"An unexpected error occured" ))
    } catch (e: IOException) {
      emit(Resource.Error("Couldn't reach server"))
    }
  }
}