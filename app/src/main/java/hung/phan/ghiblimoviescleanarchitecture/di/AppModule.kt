package hung.phan.ghiblimoviescleanarchitecture.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import hung.phan.ghiblimoviescleanarchitecture.data.remote.GhibliApi
import hung.phan.ghiblimoviescleanarchitecture.data.repository.FilmRepositoryImpl
import hung.phan.ghiblimoviescleanarchitecture.domain.repository.FilmRepository
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

  @Provides
  @Singleton
  fun providerGhibliApi() : GhibliApi {
    return Retrofit.Builder()
      .baseUrl("https://ghibliapi.herokuapp.com")
      .addConverterFactory(GsonConverterFactory.create())
      .build()
      .create(GhibliApi::class.java)
  }

  @Provides
  @Singleton
  fun provideFilmRepository(api: GhibliApi): FilmRepository {
    return FilmRepositoryImpl(api)
  }
}